Rails.application.routes.draw do
  root 'contributions#index'

  resources :contributions do
    member do
      post '/vote', to: "contributions#vote_up"
      delete '/vote', to: "contributions#unvote"

      resources :comments
    end
  end

  resources :comments do
    member do
      post '/vote', to: "comments#vote_up"
      delete '/vote', to: "comments#unvote"

      resources :comments
    end
  end

  resources :users do
    member do
      get '/contributions', to: "users#contributions"
      get '/voted_contributions', to: "users#voted_contributions"
      get '/comments', to: "users#comments"
      get '/voted_comments', to: "users#voted_comments"
    end
  end

  get '/submit', to: "contributions#new"
  get '/newest', to: "contributions#newest"
  get '/ask', to: "contributions#ask"

  # Authentication
  post '/login', to:'sessions#authenticate'

  namespace :api do
    resources :users, only: %i(index update show) do
      member do
        get '/contributions', to: "users#contributions"
        get '/voted_contributions', to: "users#voted_contributions"
        get '/comments', to: "users#comments"
        get '/voted_comments', to: "users#voted_comments"
      end
    end

    resources :contributions do
      member do
        post '/vote', to: "contributions#vote_up"
        delete '/vote', to: "contributions#unvote"
        post '/comments', to: "comments#createFromContribution"
      end
    end

    resources :comments, only: %i(show update) do
      member do
        post '/vote', to: "comments#vote_up"
        delete '/vote', to: "comments#unvote"
        post '/replies', to: "comments#createFromComment"
      end
    end
  end
end

