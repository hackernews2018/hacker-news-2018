json.collection @comments.each do |comment|
  json.partial! 'api/comments/show', resource: comment
end