json.extract! resource, :id, :text, :commentable_type, :commentable_id, :user_id, :created_at, :updated_at
json.votes resource.get_upvotes.size
json.comments_ids resource.comments.pluck(:id)