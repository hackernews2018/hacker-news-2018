json.collection @contributions.each do |contribution|
  json.partial! 'api/contributions/show', resource: contribution
end