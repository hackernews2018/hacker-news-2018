json.extract! resource, :id, :title, :user_id, :created_at, :updated_at
json.votes resource.get_upvotes.size
if resource.ask
	json.text resource.text
end

unless resource.ask
	json.url resource.url
end

json.comments_ids resource.comments.pluck(:id)
