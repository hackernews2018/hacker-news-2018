json.extract! resource, :id, :name, :email, :created_at, :updated_at, :about
if request.headers[:apikey] == resource.apikey
	json.apikey resource.apikey
end