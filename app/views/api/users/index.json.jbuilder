json.collection @users.each do |user|
  json.partial! 'api/users/show', resource: user
end