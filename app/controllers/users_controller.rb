class UsersController < ApplicationController
  before_action :set_user, only: %i(show contributions voted_contributions voted_comments comments)

  def show
  end

  def new
    @user = User.new
  end

  def contributions
    @contributions = @user.contributions
  end

  def voted_contributions
    @contributions = @user.get_voted Contribution
  end

  def comments
    @comments = Comment.where(user_id: current_user.id)
  end

  def voted_comments
    @comments = current_user.get_voted Comment
  end

  def create
    @user = User.new(user_params.merge({ apikey: params[:name].split("").shuffle.join }))

    respond_to do |format|
      if @user.save
        log_in @user
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
