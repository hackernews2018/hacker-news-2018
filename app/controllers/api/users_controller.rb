module Api
  class UsersController < ApplicationController
    def index
      @users = User.all

      respond_to do |format|
        format.json { render :index, status: :ok, resource: @users }
      end
    end

    def show
      @user = User.find(params[:id])

      respond_to do |format|
        if @user
          format.json { render :show, status: :ok, resource: @user }
        else
          format.json { render json: @user.errors, status: 404 }
        end
      end
    end

    def update
      @user = User.find(params[:id])

      if request.headers[:apikey] == @user.apikey
        respond_to do |format|
          if @user.update(user_params)
            format.json { render :show, status: :ok, resource: @user }
          else
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    def contributions
      @user = User.find(params[:id])
      @contributions = @user.contributions
      respond_to do |format|
        format.json { render 'api/contributions/index', status: :ok, resource: @contributions }
      end
    end

    def voted_contributions
      @user = User.find(params[:id])
      @contributions = @user.get_voted Contribution

      respond_to do |format|
        if request.headers[:apikey] == @user.apikey
          format.json { render 'api/contributions/index', status: :ok, resource: @contributions }
        else
          format.json { render json: { error: "Wrong api_key" }, status: :unprocessable_entity }
        end
      end
    end

    def comments
      @user = User.find(params[:id])
      @comments = Comment.where(user_id: @user.id)
      respond_to do |format|
        format.json { render 'api/comments/index', status: :ok, resource: @comments }
      end
    end

    def voted_comments
      @user = User.find(params[:id])
      @comments = @user.get_voted Comment

      respond_to do |format|
        if request.headers[:apikey] == @user.apikey
          format.json { render 'api/comments/index', status: :ok, resource: @comments }
        else
          format.json { render json: { error: "Wrong api_key" }, status: :unprocessable_entity }
        end
      end
    end

    private

    def user_params
      params.permit(:about)
    end
  end
end