module Api
  class CommentsController < ApplicationController
    def index
      if params[:type] == 'contribution'
        contribution = Contribution.find(params[:id])
        @comments = contribution.comments
      else
        comment = Comment.find(params[:id])
        @comments = comment.comments
      end

      respond_to do |format|
        format.json { render :index, status: :ok, resource: @comments }
      end
    end

    def vote_up
      @comment = Comment.find(params[:id])
      user = User.find_by(apikey: request.headers[:apikey])
      @comment.upvote_from user
      head :ok
    end

    def unvote
      @comment = Comment.find(params[:id])
      user = User.find_by(apikey: request.headers[:apikey])
      @comment.unvote_up user if user.voted_for?(@comment)
      head :ok
    end

    def show
      @comment = Comment.find(params[:id])

      respond_to do |format|
        if @comment
          format.json { render :show, status: :ok, resource: @comment }
        else
          format.json { render json: @comment.errors, status: 404 }
        end
      end
    end

    def create
      if params[:type] == 'contribution'
        contribution = Contribution.find(params[:id])
        @comment = contribution.comments.new(comment_params)
      else
        comment = Comment.find(params[:id])
        @comment = comment.comments.new(comment_params)
      end

      respond_to do |format|
        if @comment.save
          format.json { render :show, status: :created, resource: @comment }
        else
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    def createFromContribution
       contribution = Contribution.find(params[:id])
       @user = User.find_by(apikey: request.headers[:apikey])
       @comment = contribution.comments.new(comment_params.merge!({user_id: @user.id}))

      respond_to do |format|
        if @comment.save
          format.json { render :show, status: :created, resource: @comment }
        else
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    def createFromComment
      comment = Comment.find(params[:id])
      @user = User.find_by(apikey: request.headers[:apikey])
      @comment = comment.comments.new(comment_params.merge!({user_id: @user.id}))

      respond_to do |format|
        if @comment.save
          format.json { render :show, status: :created, resource: @comment }
        else
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      @comment = Comment.find(params[:id])

      if request.headers[:apikey] == @comment.user.apikey
        respond_to do |format|
          if @comment.update(comment_params)
            format.json { render :show, status: :accepted, resource: @comment }
          else
            format.json { render json: @comment.errors, status: :unprocessable_entity }
          end
        end
      end
    end


    def destroy
      @comment = Comment.find(params[:id])

      if request.headers[:apikey] == @comment.user.apikey
        respond_to do |format|
          if @comment.destroy
            format.json { head :ok }
          else
            format.json { render json: @comment.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    private

    def comment_params
      params.permit(:text)
    end
  end
end