module Api
  class ContributionsController < ApplicationController
    def index
      @contributions = Contribution.all if params[:type].blank?
      @contributions = Contribution.questions if params[:type] == 'ask'
      @contributions = Contribution.urls if params[:type] == 'url'
      @contributions = Contribution.all.sort_by { |c| -c.get_upvotes.size } if params[:type] == 'votes'
      @contributions = Contribution.all.order created_at: :desc if params[:type] == 'new'

      respond_to do |format|
        format.json { render :index, status: :ok, resource: @contributions }
      end
    end

    def vote_up
      @contribution = Contribution.find(params[:id])
      user = User.find_by(apikey: request.headers[:apikey])
      @contribution.upvote_from user if not user.voted_for?(@contribution)
      head :ok
    end

    def unvote
      @contribution = Contribution.find(params[:id])
      user = User.find_by(apikey: request.headers[:apikey])
      @contribution.unvote_up user if user.voted_for?(@contribution)
      head :ok
    end

    def show
      @contribution = Contribution.find(params[:id])

      respond_to do |format|
        if @contribution
          format.json { render :show, status: :ok, resource: @contribution }
        else
          format.json { render json: @contribution.errors, status: 404 }
        end
      end
    end

    def create
      @userid = User.find_by(apikey: request.headers[:apikey])
      @contribution = Contribution.new(contribution_params.merge!({user_id: @userid.id}))

      respond_to do |format|
        if @contribution.save
          format.json { render :show, status: :created, resource: @contribution }
        else
          format.json { render json: @contribution.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      @contribution = Contribution.find(params[:id])

      if request.headers[:apikey] == @contribution.user.apikey
        respond_to do |format|
          if @contribution.update(contribution_params)
            format.json { render :show, status: :created, resource: @contribution }
          else
            format.json { render json: @contribution.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    def destroy
      @contribution = Contribution.find(params[:id])

      if request.headers[:apikey] == @contribution.user.apikey
        respond_to do |format|
          if @contribution.destroy
            format.json { head :ok }
          else
            format.json { render json: @contribution.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    private

    def contribution_params
      params.permit(:title, :url, :text)
    end
  end
end