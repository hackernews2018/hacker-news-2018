class ContributionsController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :update, :destroy, :vote]
  before_action :set_user_id_param, only: [:create, :update]

  def vote_up
    @contribution = Contribution.find(params[:id])
    @contribution.upvote_from current_user
    redirect_back fallback_location: :fallback_location
  end

  def unvote
    @contribution = Contribution.find(params[:id])
    @contribution.unvote_up current_user
    redirect_back fallback_location: :fallback_location
  end

  def ask
    @contributions = Contribution.questions
    @contributions.sort_by {@contributions |c| -c.get_upvotes.size }
  end

  def index
    if params[:id].present?
      @contributions = Contribution.where(user_id: params[:id])
      @contributions.sort_by { |c| -c.get_upvotes.size }
    else
      @contributions = Contribution.all
      @contributions.sort_by { |c| -c.get_upvotes.size }
    end
  end

  def show
  end

  def newest
    @contributions = Contribution.all.order created_at: :desc
  end

  def new
    flash[:alert] = nil
    if current_user.present? 
        @contribution = Contribution.new
    else 
    redirect_to '/auth/google_oauth2'
     end
  end

  def create

    @contribution = Contribution.new(contribution_params.merge!({user_id: params[:contribution][:user_id]}))
    respond_to do |format|
      if @contribution.save
        format.html { redirect_to root_path, notice: 'Contribution was successfully created.' }
        format.json { render :index, status: :created, location: @contribution }
          
      elsif  @contribution.errors[:url][0] == "is invalid"
        format.html { render :new }
        flash[:alert] = "Url is invalid"
      
      elsif @contribution.errors[:base][0] == "This contribution has url and text and can only have one."
        format.html { render :new }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
        flash[:alert] = "This contribution has url and text and can only have one"
        
      elsif @contribution.errors[:title][0] == "can't be blank"
        format.html { render :new }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
        flash[:alert] = "Title can't be blank"
        
      elsif  @contribution.url != ""
        if Contribution.find_by(url: params[:contribution][:url]) != nil
          format.html {redirect_to Contribution.find_by(url: params[:contribution][:url])}
        end
        
      else
          format.html { render :new }
          format.json { render json: @contribution.errors, status: :unprocessable_entity }
          flash[:alert] = "Introduce Url or Text"
        
      end
    end
  end
  
  

  def destroy
    if @contribution.user.id = current_user.id
      @contribution.destroy
      respond_to do |format|
        format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private

    def set_contribution
      return if @contribution = Contribution.find(params[:id])

      redirect_to root_url
    end

    def set_user_id_param
      params[:contribution][:user_id] = current_user.id if current_user
    end

    def contribution_params
      params.require(:contribution).permit(:title, :url, :text)
    end
end
