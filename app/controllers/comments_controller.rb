class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_user_id_param, only: [:create, :update]

  def vote_up
    @comment = Comment.find(params[:id])
    @comment.upvote_from current_user
    redirect_back fallback_location: :fallback_location
  end

  def unvote
    @comment = Comment.find(params[:id])
    @comment.unvote_up current_user
    redirect_back fallback_location: :fallback_location
  end

  def show
  end

  def new
    @comment = Comment.new
  end

  def create
    if current_user.present?
      @commentable = Contribution.find(params[:id]) if params[:type] == 'contribution'
      @commentable = Comment.find(params[:id]) if params[:type] == 'comment'
      @comment = @commentable.comments.new(comment_params.merge!({user_id: params[:comment][:user_id]}))
      respond_to do |format|
        if @comment.save
          format.html { redirect_back fallback_location: :fallback_location, notice: 'The comment was successfully created.' }
          format.json { render :show, status: :created, location: @comment }
        else
          format.html { redirect_back fallback_location: :fallback_location }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    else 
       redirect_to '/auth/google_oauth2'
    end
  end

  def destroy
    if @comment.user.id = current_user.id
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to contributions_url, notice: 'Comment was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    def set_comment
      return if @comment = Comment.find(params[:id])
    end

    def set_user_id_param
      params[:comment][:user_id] = current_user.id if current_user
    end

    def comment_params
      params.require(:comment).permit(:text, :type)
    end
end

