class Comment < ApplicationRecord
  acts_as_votable
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :comments, as: :commentable, dependent: :destroy

  validates :text, presence: true
end
