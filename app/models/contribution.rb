class Contribution < ApplicationRecord
  acts_as_votable

  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy

  REGEX_URL = /\Ahttps?:\/\/([^\/]+)\/?/
  validate :text_url_presence
  validates :user, :title, presence: true
  validates :url, uniqueness: true, if: :url?
  validates :url, format: { with: REGEX_URL }, on: :create, if: :url?

  scope :urls, -> { where(ask: false) }
  scope :questions, -> { where(ask: true) }

  def get_from
    REGEX_URL.match(url) ? REGEX_URL.match(url)[1] : ""
  end

  private

  def text_url_presence
    unless text.present? && url.present?
      set_ask
      return
    end

    errors.add :base, "This contribution has url and text and can only have one."
  end

  def set_ask
    update_attribute(:ask, text.present?)
  end

  def url?
    ask == false;
  end
end
