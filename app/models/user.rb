class User < ApplicationRecord
  acts_as_voter
  before_save { self.email = email.downcase }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  has_many :contributions

  validates :name, :email, :apikey, presence: true
  validates :email, uniqueness: true
  validates :email, format: { with: VALID_EMAIL_REGEX }

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first
    unless user
      user = User.create(name: data['name'],
                         email: data['email'],
                         apikey: data['name'].split("").shuffle.join.gsub(' ', ''))
    end

    user
  end

  def self.create_user_for_google(data)
    where(email: data["email"]).first_or_initialize.tap do |user|
      user.name=data["name"]
      user.email=data["email"]
      user.apikey=data["at_hash"]
      user.save!
    end
  end

end
