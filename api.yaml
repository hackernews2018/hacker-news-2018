swagger: '2.0'
info:
  description: "Hacker news API"
  version: "1.0.0"
  title: "Hackernews"
host: "mighty-waters-44524.herokuapp.com"
basePath: "/api/"
tags:
  - name: "users"
    description: "User related actions"
  - name: "contributions"
    description: "Questions or URLs"
  - name: "comments"
    description: "Comments on contributions or other comments"
schemes:
  - "https"
paths:
  /users.json:
    get:
      tags:
        - "users"
      summary: "Returns the a list of users"
      description: "Returns the a list of users"
      operationId: "findUsers"
      produces:
        - "application/json"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/UserCollection'
        '204':
          description: "No users were found"
  /users/{userId}.json:
    get:
      tags:
        - "users"
      summary: "Returns one user"
      description: "Returns one user"
      operationId: "findOneUser"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          description: "User ID"
          type: "integer"
          format: "int64"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/User'
        '404':
          description: "No user was found with this id"
          schema:
            type: "object"
            properties:
              status:
                type: "integer"
              error:
                type: "string"
    put:
      tags:
        - "users"
      summary: "Update user info"
      description: "Update user info"
      operationId: "updateUser"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          description: "User ID"
          type: "integer"
          format: "int64"
        - name: "user"
          in: "body"
          required: true
          description: "User info to update"
          schema:
            $ref: '#/definitions/UserSubmission'
      responses:
        '200':
          description: "User updated successfully"
          schema:
            $ref: '#/definitions/User'
        '400':
          description: "Wrong parameters sent"
        '401':
          description: "Needs authentication to update user"
        '403':
          description: "You cannot update this user"
        '404':
          description: "This user id doesn't exist"
      security:
        - apikey: []
  /users/{userId}/contributions.json:
    get:
      tags:
        - "users"
      summary: "Returns the a list of contributions"
      description: "Returns the a list of contributions"
      operationId: "findUserContributions"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          type: "integer"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/ContributionCollection'
        '204':
          description: "No users were found"
  /users/{userId}/voted_contributions.json:
    get:
      tags:
        - "users"
      summary: "Returns the a list of contributions"
      description: "Returns the a list of contributions"
      operationId: "findUserVotedContributions"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          type: "integer"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/ContributionCollection'
        '204':
          description: "No users were found"
      security:
        - apikey: []
  /users/{userId}/comments.json:
    get:
      tags:
        - "users"
      summary: "Returns the a list of comments"
      description: "Returns the a list of comments"
      operationId: "findUserComments"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          type: "integer"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/CommentCollection'
        '204':
          description: "No users were found"
  /users/{userId}/voted_comments.json:
    get:
      tags:
        - "users"
      summary: "Returns the a list of comments"
      description: "Returns the a list of comments"
      operationId: "findUserVotedComments"
      produces:
        - "application/json"
      parameters:
        - name: "userId"
          in: "path"
          required: true
          type: "integer"
      responses:
        '200':
          description: "successful operation"
          schema:
            $ref: '#/definitions/CommentCollection'
        '204':
          description: "No users were found"
      security:
        - apikey: []
  /contributions.json:
    get:
      tags:
        - contributions
      summary: Returns a list of contributions
      description: Returns a list of contributions
      operationId: findContributions
      produces:
        - application/json
      parameters:
        - name: type
          in: query
          description: Type of find request
          required: false
          type: string
          enum:
            - ask
            - url
            - votes
            - new
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/ContributionCollection'
        '204':
          description: No Contributions found
        '400':
          description: Not valid parameters
    post:
      tags:
        - contributions
      summary: Submit a new contribution
      description: Submit a new contribution
      operationId: createContribution
      produces:
        - application/json
      parameters:
        - in: body
          name: contribution
          description: Contribution to submit
          required: true
          schema:
            $ref: '#/definitions/ContributionSubmission'
      responses:
        '201':
          description: successful operation
          schema:
            $ref: '#/definitions/Contribution'
        '400':
          description: Invalid parameters
        '401':
          description: Must be a logged in user to perform this action
      security:
        - apikey: []
  '/contributions/{contributionId}.json':
    get:
      tags:
        - contributions
      summary: Returns a single contribution
      description: Returns a single contribution
      operationId: findOneContribution
      produces:
        - application/json
      parameters:
        - name: contributionId
          in: path
          description: ID of the contribution
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Contribution'
        '404':
          description: Contribution not found
    put:
      tags:
        - contributions
      summary: Submit an existing contribution
      description: Submit an existing contribution
      operationId: updateContribution
      produces:
        - application/json
      parameters:
        - name: contributionId
          in: path
          description: ID of the contribution
          required: true
          type: integer
          format: int32
        - in: body
          name: contribution
          description: Contribution to submit
          required: true
          schema:
            $ref: '#/definitions/ContributionSubmission'
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Contribution'
        '400':
          description: Invalid parameters
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You are not allowed to update this contribution
      security:
        - apikey: []
    delete:
      tags:
        - contributions
      summary: Delete an existing contribution
      description: Delete an existing contribution
      operationId: destroyContribution
      produces:
        - application/json
      parameters:
        - name: contributionId
          in: path
          description: ID of the contribution
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Contribution'
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You are not allowed to destroy this contribution
        '404':
          description: Contribution not found
      security:
        - apikey: []
  '/contributions/{contributionId}/vote.json':
    post:
      tags:
        - contributions
      summary: Votes a contribution positively
      description: Votes a contribution positively
      operationId: voteUpContribution
      produces:
        - application/json
      parameters:
        - name: contributionId
          in: path
          description: ID of the contribution
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Vote'
        '400':
          description: The contribution id is not valid
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You cannot upvote yourself
        '409':
          description: You already upvoted this contribution
      security:
        - apikey: []
    delete:
      tags:
        - contributions
      summary: Removes a vote from a contribution
      description: Removes a vote from a contribution
      operationId: unvoteContribution
      produces:
        - application/json
      parameters:
        - name: contributionId
          in: path
          description: ID of the contribution
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Vote'
        '400':
          description: The contribution id is not valid
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You cannot unvote yourself
        '409':
          description: You cannot unvote a contribution that you didn't vote
      security:
        - apikey: []
  '/comments/{commentId}.json':
    get:
      tags:
        - comments
      summary: Returns a single comment
      description: Returns a single comment
      operationId: findOneComment
      produces:
        - application/json
      parameters:
        - name: commentId
          in: path
          description: ID of the comment
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Comment'
        '404':
          description: Comment not found
    put:
      tags:
        - comments
      summary: Submit an existing comment
      description: Submit an existing comment
      operationId: updateComment
      produces:
        - application/json
      parameters:
        - name: commentId
          in: path
          description: ID of the comment
          required: true
          type: integer
          format: int32
        - in: body
          name: comment
          description: Comment to submit
          required: true
          schema:
            $ref: '#/definitions/CommentSubmission'
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Comment'
        '400':
          description: Invalid parameters
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You are not allowed to update this comment
      security:
        - apikey: []
    delete:
      tags:
        - comments
      summary: Delete an existing comment
      description: Delete an existing comment
      operationId: destroyComment
      produces:
        - application/json
      parameters:
        - name: commentId
          in: path
          description: ID of the comment
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Comment'
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You are not allowed to destroy this comment
        '404':
          description: Comment not found
      security:
        - apikey: []
  '/comments/{commentId}/vote.json':
    post:
      tags:
        - comments
      summary: Votes a comment positively
      description: Votes a comment positively
      operationId: voteUpComment
      produces:
        - application/json
      parameters:
        - name: commentId
          in: path
          description: ID of the comment
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Vote'
        '400':
          description: The comment id is not valid
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You cannot upvote yourself
        '409':
          description: You already upvoted this contribution
      security:
        - apikey: []
    delete:
      tags:
        - comments
      summary: Removes a vote from a comment
      description: Removes a vote from a comment
      operationId: unvoteComment
      produces:
        - application/json
      parameters:
        - name: commentId
          in: path
          description: ID of the comment
          required: true
          type: integer
          format: int32
      responses:
        '200':
          description: successful operation
          schema:
            $ref: '#/definitions/Vote'
        '400':
          description: The comment id is not valid
        '401':
          description: Must be a logged in user to perform this action
        '403':
          description: You cannot unvote yourself
        '409':
          description: You cannot unvote a comment that you didn't vote
      security:
        - apikey: []
  '/contributions/{contributionId}/comments.json':
    post:
      tags:
        - comments
      summary: Submit a new comment
      description: Submit a new comment
      operationId: createComment
      produces:
        - application/json
      parameters:
        - in: path
          name: contributionId
          required: true
          description: ID of contribution
          type: integer
          format: int64
        - in: body
          name: comment
          description: Comment to submit
          required: true
          schema:
            $ref: '#/definitions/CommentSubmission'
      responses:
        '201':
          description: successful operation
          schema:
            $ref: '#/definitions/Comment'
        '400':
          description: Invalid parameters
        '401':
          description: Must be a logged in user to perform this action
      security:
        - apikey: []
  '/comments/{commentId}/replies.json':
    post:
      tags:
        - comments
      summary: Submit a new comment
      description: Submit a new comment
      operationId: createReply
      produces:
        - application/json
      parameters:
        - in: path
          name: commentId
          required: true
          description: ID of comment
          type: integer
          format: int64
        - in: body
          name: comment
          description: Comment to submit
          required: true
          schema:
            $ref: '#/definitions/CommentSubmission'
      responses:
        '201':
          description: successful operation
          schema:
            $ref: '#/definitions/Comment'
        '400':
          description: Invalid parameters
        '401':
          description: Must be a logged in user to perform this action
      security:
        - apikey: []
securityDefinitions:
  apikey:
    type: apiKey
    name: apikey
    in: header
definitions:
  User:
    type: object
    properties:
      id:
        type: integer
        format: int64
      name:
        type: string
      email:
        type: string
      about:
        type: string
      created_at:
        type: string
        format: date-time
      updated_at:
        type: string
        format: date-time
  UserCollection:
    type: object
    properties:
      collection:
        type: array
        items:
          $ref: '#/definitions/User'
  UserSubmission:
    type: object
    properties:
      about:
        type: string
  Contribution:
    type: object
    properties:
      id:
        type: integer
        format: int64
      title:
        type: string
      user:
        $ref: '#/definitions/User'
      url:
        type: string
      text:
        type: string
      ask:
        type: boolean
      created_at:
        type: string
        format: date-time
      updated_at:
        type: string
        format: date-time
      comments:
        type: array
        items:
          $ref: '#/definitions/Comment'
  ContributionCollection:
    type: object
    properties:
      collection:
        type: array
        items:
          $ref: '#/definitions/Contribution'
  ContributionSubmission:
    type: object
    properties:
      title:
        type: string
      url:
        type: string
      text:
        type: string
  Comment:
    type: object
    properties:
      id:
        type: integer
        format: int64
      text:
        type: string
      user:
        $ref: '#/definitions/User'
      commentable_type:
        type: string
      commentable_id:
        type: integer
        format: int64
      created_at:
        type: string
        format: date-time
      updated_at:
        type: string
        format: date-time
      comments:
        type: array
        items:
          $ref: '#/definitions/Comment'
  CommentCollection:
    type: object
    properties:
      collection:
        type: array
        items:
          $ref: '#/definitions/Comment'
  CommentSubmission:
    type: object
    properties:
      text:
        type: string
  Vote:
    type: object
    properties:
      id:
        type: integer
        format: int64
      votable_id:
        type: integer
        format: int64
      voter_id:
        type: integer
        format: int64
      votable_type:
        type: string
      voter_type:
        type: string
      vote_flag:
        type: boolean
      vote_scope:
        type: string
      vote_weight:
        type: integer
        format: int64
      created_at:
        type: string
        format: date-time
      updated_at:
        type: string
        format: date-time
