class ChangeAskType < ActiveRecord::Migration[5.2]
  def change
    Contribution.delete_all
    Contribution.reset_column_information
    change_column :contributions, :ask, :boolean, null: false
  end
end
