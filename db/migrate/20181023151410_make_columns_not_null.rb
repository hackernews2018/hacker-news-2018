class MakeColumnsNotNull < ActiveRecord::Migration[5.2]
  def change
    
    change_column :users, :email, :string, null: true, unique: true
    change_column :users, :name, :string, null: true, unique: true
    
  end
end
