class CreateContributions < ActiveRecord::Migration[5.2]
  def change
    create_table :contributions do |t|

      t.timestamps
      t.string :title, null: false
      t.string :url
      t.text :text
      t.boolean :ask, null: false
      t.belongs_to :user, index: true
      
    end
  end
end
