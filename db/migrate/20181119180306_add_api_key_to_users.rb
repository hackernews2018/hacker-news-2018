class AddApiKeyToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :apikey, :string

    User.reset_column_information()
    User.all.each do |user|
      user.apikey = user.name.split("").shuffle.join
    end

    change_column_null :users, :apikey, false
  end
end
