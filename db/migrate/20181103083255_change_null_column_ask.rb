class ChangeNullColumnAsk < ActiveRecord::Migration[5.2]
  def change
    change_column :contributions, :ask, :string, null: true
  end
end
